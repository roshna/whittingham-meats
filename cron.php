<?php

error_reporting(E_ALL);

ini_set('memory_limit', '-1');

ini_set("display_errors", 1);

require __DIR__ . '/vendor/autoload.php';
require_once  dirname(__FILE__). '/model/shopify_product.php';
require_once  dirname(__FILE__) .'/lib/Logging.php';


$logDirPath   = dirname(__FILE__).'/logs/cron/';
$fileToLog    = $logDirPath.'product_import_log.txt';
if (! file_exists($fileToLog)) {
    touch($fileToLog);
    // Make it writeable
    chmod($fileToLog, 0777);
}

$successFile    = $logDirPath.'successProducts.csv';
if (! file_exists($successFile)) {
    touch($successFile);
    // Make it writeable
    chmod($successFile, 0777);
}
$successFileHandle = fopen($successFile, 'w');

$errorFile    = $logDirPath.'errorProducts.csv';
if (! file_exists($errorFile)) {
    touch($errorFile);
    // Make it writeable
    chmod($errorFile, 0777);
}
$errorFileHandle = fopen($errorFile, 'w');


$shopifyProductDao  = new shopifyProduct();
$successCount = 0;
$errorCount   = 0;
$totalCount   = 0;
global $fileToLog;
global $successCount;
global $errorCount;
global $totalCount;
global $shopifyProductDao;
global $successFileHandle;
global $errorFileHandle;
global $executionFile;


$executionTime = date('Y-m-d H:i:s');
$lastExecutionFile = $logDirPath.'executionFile.txt';
if (! file_exists($lastExecutionFile)) {
    touch($lastExecutionFile);
    // Make it writeable
    chmod($lastExecutionFile, 0777);
}

$readHandle = fopen ($lastExecutionFile, "r");

$fileData   = fgetcsv($readHandle);

if(!empty($fileData))
{

    while(!feof($readHandle)){
        if($fileData[1] == 'Working' ){

            $currentTime     = strtotime(date('Y-m-d H:i:s'));
            $previousTime    = strtotime($fileData[0]);
            $differenceInSec =  $currentTime -  $previousTime ;
            $diffInHours     = ($differenceInSec)/60/60;

            if ($diffInHours <= 24){
                logEntry('existing cron job working stopped execution...');
                die('an existing cron job working');

            } else {

                logEntry('Started Processing again' );
                process();
            }
        } else if ($fileData[1] != 'Done' && $fileData[1] != 'Start' ){

            logEntry("Error intiating cron job");
            exit();

        } else {

            logEntry('Started processing again ....');
            process();

        }
    }

}
else
{

    logEntry('Started Processing from beginning');
    process();

}

$writeHandle   = fopen ($lastExecutionFile, "w");
$executionTime = date('Y-m-d H:i:s');
fputcsv($writeHandle,array($executionTime,"Done"));
fclose($writeHandle);





/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(array(
                        Google_Service_Sheets::SPREADSHEETS_READONLY,
                        Google_Service_Sheets::DRIVE,
                        Google_Service_Sheets::DRIVE_FILE,
                        ));
    
    // $client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);

    $client->setAuthConfig(dirname(__FILE__).'/client_secret.json');
    
    $client->setAccessType('offline');

    // Load previously authorized credentials from a file.
    $credentialsPath = expandHomeDirectory('credentials.json');
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        // Store the credentials to disk.
        if (!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path)
{
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}



function process()
{
    logEntry('proicesssss');

    // Get the API client and construct the service object.    
    global $fileToLog;
    global $successCount;
    global $errorCount;
    global $totalCount;
    global $shopifyProductDao;
    global $successFileHandle;
    global $errorFileHandle;
    global $lastExecutionFile;


    $writeHandle   = fopen ($lastExecutionFile, "w");
    $executionTime = date('Y-m-d H:i:s');
    fputcsv($writeHandle,array($executionTime,"Working"));
    fclose($writeHandle);




    $client       = getClient();
    $service      = new Google_Service_Sheets($client);
    $driveService = new Google_Service_Drive($client);

    $driveFileService = new Google_Service_Drive_DriveFile($client);

    // Print the names and IDs for up to 10 files.
    $folderId = '1Xhs4bguNUZilrHFYocMaEdgNXop6mpzm';   //"1Xhs4bguNUZilrHFYocMaEdgNXop6mpzm";    //14BpNuEj5-T0Qzqx9jxMYG4FvIxCcd5ut

    $optParams = array(
      // 'pageSize' => 10,
      'fields' => 'nextPageToken, files(id, name)',
      'q' => "'".$folderId."' in parents"
    );

    $results = $driveService->files->listFiles($optParams);
    // print_r($results);die;

    if(isset($results['files']))
    {
        foreach($results['files'] as $file)
        {
            
            logEntry('Processing each files in folder');
            $fileId   = $file['id'];
            $fileName = substr($file['name'], 0, 20);   

            // if ($fileName == "Master Price List" || $fileName == "110 Grill") {     //110 Grill  //12 Handles Ale House
            //     continue;
            // }
            // echo $fileName;die;

            // Prints the names and majors of students in a sample spreadsheet:
            // https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
            $spreadsheetId = $fileId;  
            $range    = 'A1:Z300';    //'Class Data!A2:E';
            $response = $service->spreadsheets_values->get($spreadsheetId, $range);
            $values   = $response->getValues();
            // logEntry(print_r($values,1));
            // print_r($values);die;

            if(!empty($values))
            {

                $dataArray = array();
                $foundData = false;
                $headArray = array();
                foreach($values as $key => $value)
                {

                    if($foundData == false)
                    {

                        if(in_array("Description", $value))
                        {
                            $trimmedHeading = array_map('trim',$value);
                            array_push($headArray, $trimmedHeading);
                            $foundData = true;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if(!empty($value))
                        {   
                            // print_r($headArray);
                            // print_r($value);die;
                            for($i=0;$i<count($value);$i++)
                            {
                                // printr_r($value);die;
                                // if(count($value) == count($headArray))
                                // {
                                    $data[trim($headArray[0][$i])] = trim($value[$i]);    
                                // }
                                
                            }
                            // print_r($data);die;

                            array_push($dataArray, $data);
                        }
                        else 
                            break;
                    }

                }

                // print_r($dataArray);die;
                

                $heading  = array_values($headArray[0]);

                foreach ($dataArray as $key => $data) 
                {
                    
                    if(array_key_exists('Description', $data))
                    {
                        $description = $data['Description'];
                    }

                    if(array_key_exists('Specifications', $data))
                    {
                        $specifications = $data['Specifications'];
                    }

                    if(array_key_exists('Price per lbs.', $data))
                    {
                        $pricePerLbs = $data['Price per lbs.'];
                    }

                    if(array_key_exists('Grade', $data))
                    {
                        $grade = $data['Grade'];
                    }

                    if(array_key_exists('Price', $data))
                    {
                        $price = $data['Price'];
                    }

                    if(array_key_exists('SKU', $data))
                    {
                        $sku = $data['SKU'];
                    }


                    $metaPrice = '';
                    if(!empty($pricePerLbs))
                    {
                        $metaPrice = $pricePerLbs;
                    }
                    else if(!empty($price))
                    {
                        $metaPrice = $price;   
                    }
                    else if (!empty($pricePerLbs) && !empty($price))
                    {
                        $metaPrice = $pricePerLbs;
                    }

                    $totalCount ++;
                    
                    logEntry('Processing the product sku - '.$sku);
                    echo PHP_EOL."Processing the product with SKU - ".$sku;

                    $result = getProductDetailsWithSKU($sku);
                    // print_r($result);die;
                    $productDetails = $result->data->shop->products->edges;

                    // print_r($productDetails);die;
                    if(!empty($productDetails))
                    {
                        $productId = getProductId($productDetails[0]->node->id);
                        $variantId = getVariantId($productDetails[0]->node->variants->edges,$sku);

			logEntry('Calling metafield update call');
                        echo "   => Calling metafield update call";
                        
                        $metaResult = setProductMetaField($variantId, $metaPrice, $fileName, $shopifyProductDao);

                        // print_r($metaResult);die;
                        if(isset($metaResult['errors']))
                        {
                            $errorCount ++;
                            $errorMessage = '';

                            foreach($metaResult['errors'] as $key  => $value) 
                            {
                                $errorMessage = $errorMessage." ".$key. " ".$value[0].", ";
                            }
				logEntry('error in metafield update - '.$errorMessage);
                            fputcsv($errorFileHandle, array($sku,$errorMessage));
                        }
                        else 
                        {
                            $successCount ++;
			    logEntry('Sucess in calling metafield');
                            fputcsv($successFileHandle, array($sku,'Success'));
                        }


                    }
                    else 
                    {

                        logEntry('NO Such Product - '.$sku);
                        $errorMessage = 'No Such Product in shopify';
                        $errorCount ++;
                        fputcsv($errorFileHandle, array($sku,$errorMessage));
                        
                    }
                }
                
            }

        }
    }
    else
    {
        logEntry('NO Files in the folder');
        die('NO Files in the folder');
    }


    echo PHP_EOL.'######## Summary ##########';
    echo PHP_EOL.'Total records in file - '.$totalCount;
    echo PHP_EOL.'Total success records - '.$successCount;
    echo PHP_EOL.'Total error records   - '.$errorCount;

    logEntry('######## Summary ##########');
    logEntry('Total records in file - '.$totalCount);
    logEntry('Total success records - '.$successCount);
    logEntry('Total error records   - '.$errorCount);


}



die;

function getProductDetailsWithSKU($sku)
{
    $value = callGraphQlCurl($sku);
    return json_decode($value);
}

function callGraphQlCurl($sku){
  $curl = curl_init();

  $dataParams = getDataParams($sku);

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://p80w-devstore-5.myshopify.com/admin/api/graphql.json",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $dataParams,
    CURLOPT_HTTPHEADER => array(
      "cache-control: no-cache",
      "content-type: application/graphql;charset=UTF-8",
      "X-Shopify-Access-Token: bbe2bc43ad20a0f00b667f04364f30bc"
    ),
  ));

  $response = curl_exec($curl);
  curl_close($curl);


  return $response;

}

function getDataParams($sku){

  // $sku = 'orangatang-cap-5-panel-otang-logo-hat';

  $dataParams = '{
    shop {
      products(first: 1, query:"sku:'.$sku.'") {
        edges {
          node {
            id
            handle
            title
            tags
            variants(first: 100) {
              edges {
                node {
                  id
                  sku
                }
              }
            }
          }
          cursor
        }
        pageInfo {
          hasNextPage
        }
      }
    }
  }';

  // echo PHP_EOL.$dataParams.PHP_EOL;

  return $dataParams;

} 

function getProductId($data)
{
    $dataSplit = explode("/", $data);
    $productId = end($dataSplit);

    return $productId;
}

function getVariantId($data,$sku)
{
    
    $variantId = '';
    
    foreach($data as $value)
    {
        if($value->node->sku === $sku)
        {
            $variantId = getProductId($value->node->id);
        }
        else
        {
            continue;
        }
    }
    return $variantId;

}

function setProductMetaField($variantId, $price, $fileName, $shopifyProductDao)
{
    $data = array();
    $data = array(
       
        //'variant' => array(
            //id' => $variantId,
            'metafield' => array(

                'key'        => str_replace(' ','_',$fileName),
                'value'      => $price,
                'value_type' => 'string',
                'namespace'  => 'wsl'
            )
        );
    //);

    $shopifyResult = $shopifyProductDao->createVariantMetafield($variantId,$data);
    return $shopifyResult;
}

function logEntry($message)
{
    
    global $fileToLog;

    $log = new Logging();
    $log->lfile($fileToLog);
    $log->lwrite($message, true);
    $log->lclose();
}

