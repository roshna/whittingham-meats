<?php 

require_once dirname(__FILE__).'/shopify_main.php';

class shopifyProduct{
	
	private  $shopifyObj ;
	private $attempt;
	
	function __construct(){
	
		$this->shopifyObj = new shopify();
		$this->attempt  = 0;
		
	//	echo'<pre>';
			
		//print_r($this->getShopifyproducts());
	}
	
	/**
	 * Getting shopify products based on page no. and limit in api call
	 */
	function getShopifyproducts($pageNo = NULL , $limit = NULL){
		
		$method = 'GET';
		$path   = '/admin/products.json?limit='.$limit.'&page='.$pageNo.'&fields=id,title,product_type,tags,variants';
		$params = '';
        $done   = true;

        do {

			try{

				$products = $this->shopifyObj->shopifyClientObj->call($method, $path);

				if($products){
			 		return $products;
				}


			}catch(Exception $e){
	           	$done = false;
			    $this->attempt ++;
			}

       	} while(!$done && $this->attempt < 3);

        return ($this->attempt <= 3 ? "error" : null);
		
	}


	function getShopifyProductsCount(){
		
		$method = 'GET';
		$path   = '/admin/products/count.json';
		$params = '';
		
		$productsCount = $this->shopifyObj->shopifyClientObj->call($method, $path);
		if($productsCount){

			return  $productsCount;
		}
		return null;
	}


	function getShopifyProductWithTitle($title = NULL ){
		
		$method = 'GET';
		$path   = '/admin/products.json?title='.$title;
		$params = '';
		
		$products = $this->shopifyObj->shopifyClientObj->call($method, $path);
		if($products){

			return  $products;
		}
		return null;
	}


	function getShopifyProductWithSku($sku){
		
		$method = 'GET';
		$path   = '/admin/products.json?sku='.$sku;
		$params = '';
		
		$products = $this->shopifyObj->shopifyClientObj->call($method, $path);
		if($products){

			return  $products;
		}
		return null;
	}

	function getShopifyproductById($id){
		
		$method = 'GET';
		$path   = '/admin/products.json?id='.$id;
		$params = '';
		
		$products = $this->shopifyObj->shopifyClientObj->call($method, $path);
		if($products){

			return  $products;
		}
		return null;
	}
	
	function createShopifyproducts($request){
		
		$method = 'POST';
		$path   = '/admin/products.json';
		$params = $request;
		
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
		
		if($response){
		
			return  $response;
		}
		return null;
		
		
	}
	function updateShopifyproducts($shopifyProductId,$request){
	
		
		$method  = 'PUT';
		$path    = '/admin/products/'.$shopifyProductId.'.json';
		$params =  $request;
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
		// 		echo '<pre>';
		// 		print_r($response);
		return $response;
	
	}
	function importImage($shopifyProductId,$requestArray){
	
	
		$method  = 'POST';
		$path    = '/admin/products/'.$shopifyProductId.'/images.json';
		$params =  $requestArray;
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
		// 		echo '<pre>';
		// 		print_r($response);
		return $response;
	
	}
	function createProductMetaField($productId,$data){
	
		$method = 'POST';
		$path   = '/admin/products/'.$productId.'/metafields.json';
		$params = $data;
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
				// echo '<pre>';
				// print_r($response);die('******');
		return $response;
	
	}
	

	function deleteShopifyProduct($id = NULL ){
		
		$method = 'GET';
		$path   = '/admin/products.json?id='.$id;
		$params = '';
		
		$products = $this->shopifyObj->shopifyClientObj->call($method, $path);
		if($products){

			return  $products;
		}
		return null;
	}

	function createVariantMetafield($id, $data){
		
		$method = 'POST';
		$path   = '/admin/variants/'.$id.'/metafields.json';
		$params = $data;
		
		$products = $this->shopifyObj->shopifyClientObj->call($method, $path, $params);
		if($products){

			return  $products;
		}
		return null;
	}
}

//new shopifyProduct();
