<?php

require_once dirname(__FILE__).'/shopify_main.php';

class shopifyCustomer{
	
	
	private $shopifyObj ;
	
	function __construct(){
		
		$this->shopifyObj = new shopify();
		 
	}
	
	
	/**
	 * Getting customerDetail using email id
	 */
	function getCustomerDetail($email = null){
		
		if ($email){			
			$method = 'GET';
			$path   = '/admin/customers/search.json?query=email:'.$email;
			$params = '';
			$customer = $this->shopifyObj->shopifyClientObj->call($method,$path);
			if ($customer){
				
				return $customer;
			}
		}
		return null;
	}
	
	/**
	 * Getting customer details in bullk
	 */
	function getAllCustomer($pageNo,$limit){
		
		$method = 'GET';
		
		$path   = '/admin/customers.json?limit='.$limit.'&page='.$pageNo;
		
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path);
		
		if($response){
			return $response;
		}
		return null;
	}
	
	/**
	 * Getting All customer order based on id
	 */
	function getAllCustomerOrders($customerId){
		
		$method = 'GET';
		$path   = '/admin/orders.json?customer_id='.$customerId;
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path);	
		if($response){
			return $response;
		}
		return null;
	}
	
	/**
	 * 
	 * @param unknown $updateCustomer
	 * @param unknown $customerId
	 * @return unknown|mixed|NULL
	 */
	function updateCustomerDetails($updateCustomer,$customerId){
		
	
		$method = 'PUT';
		$params = $updateCustomer;
		$path   = '/admin/customers/'.$customerId.'.json';
		
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
		
		if($response){
			return $response;
		}
		return null;
		
		
	}
	
	/**
	 * 
	 * @param unknown $data
	 * @return unknown|mixed|NULL
	 */
	function createCustomer($data){
	
		$method = 'POST';
		$params = $data;
		$path  = '/admin/customers.json';
	
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
	
		if($response){
			return $response;
		}
		return null;
	
	}
	
	/**
	 * delete customer address
	 *
	 * @param unknown $customerId
	 * @param unknown $addressId
	 * @return unknown|mixed|NULL
	 */
	function deleteCustomerAddress($addressId, $customerId){
	    
	    $method = 'DELETE';
	    $path   = '/admin/customers/' . $customerId . '/addresses/' . $addressId . '.json ';
	    
	    $response = $this->shopifyObj->shopifyClientObj->call($method, $path);
	    
	    if($response){
	        return $response;
	    }
	    return null;
	}
	
	/**
	 * 
	 * @param unknown $addressId
	 * @param unknown $customerId
	 * @return unknown|mixed|NULL
	 */
	function setDefultCustomerAddress($addressId, $customerId){
	    
	    $method = 'PUT';
	    $path   = '/admin/customers/' . $customerId . '/addresses/' . $addressId . '/default.json ';
	    
	    $response = $this->shopifyObj->shopifyClientObj->call($method, $path);
	    
	    if($response){
	        return $response;
	    }
	    return null;
	}
	
	function createNewMetafield($customerId,$request){
       
        $method = 'POST';
        $path   = '/admin/customers/'.$customerId.'/metafields.json';
        $params = $request;
        $response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
        return $response;
    }

	function deleteCustomer($customerId){
       
        $method = 'DELETE';
        $path   = '/admin/customers/'.$customerId.'.json';
        $response = $this->shopifyObj->shopifyClientObj->call($method, $path);
        return $response;
    }
}

