<?php 

require_once dirname(__FILE__).'/../lib/shopify.php';
require_once dirname(__FILE__).'/../config/shopify_config.php';


class shopify{
	
	protected $shopifyPass;
	protected $shopifyApiKey;
	protected $shopifySecreat;
	protected $shopifyDomin;
	public    $shopifyClientObj;
	
	function __construct(){
		$this->getShopifyConfig();
	
		$this->shopifyClientObj = new ShopifyClient($this->shopifyDomin,
				$this->shopifyPass,
				$this->shopifyApiKey,
				$this->shopifyPass);
	}
	/**
	 * Getting config of shopify
	 */
	function getShopifyConfig(){
	
		$shopifyConfigObj = new shopifyConfig();
	
		$this->shopifyApiKey  = $shopifyConfigObj->apiKey;
		$this->shopifyPass    = $shopifyConfigObj->password;
		$this->shopifySecreat = $shopifyConfigObj->secret;
		$this->shopifyDomin   = $shopifyConfigObj->shopDomain;
	}
	
	
}