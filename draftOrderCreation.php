<?php
header("Access-Control-Allow-Headers:Origin,X-Request-With,Content-Type, Accept");

//require __DIR__ . '/vendor/autoload.php';
require_once  dirname(__FILE__). '/model/shopify_product.php';
require_once  dirname(__FILE__). '/model/shopify_order.php';
require_once  dirname(__FILE__) .'/lib/Logging.php';


$logDirPath   = dirname(__FILE__).'/logs/';
$fileToLog    = $logDirPath.'order_creation.txt';
if (! file_exists($fileToLog)) {
    touch($fileToLog);
    // Make it writeable
    chmod($fileToLog, 0777);
}

$successFile    = $logDirPath.'successOrders.csv';
if (! file_exists($successFile)) {
    touch($successFile);
    // Make it writeable
    chmod($successFile, 0777);
}
$successFileHandle = fopen($successFile, 'w');

$errorFile    = $logDirPath.'skippedOrders.csv';
if (! file_exists($errorFile)) {
    touch($errorFile);
    // Make it writeable
    chmod($errorFile, 0777);
}
$errorFileHandle = fopen($errorFile, 'w');


$shopifyProductDao  = new shopifyProduct();
$shopifyOrderDao    = new shopifyOrder();

global $fileToLog;


/*$_POST = array(
    'email'=>'test@test.com',
    'line_items'=> array(
           [
             'title' => 'Aspire Cleito 120 Tank',
             'quantity'=>'2',
             'price'=>'15.00'
           ],
           [
              'title' => 'Cleito Aspire',
             'quantity'=>'3',
             'price'=>'25.00'
           ]
    ),
    "billing_address" => array (
  		"address1" => "123 Amoebobacterieae St",
  		"address2" =>  "",
  		"city"=>  "Ottawa",
  		"company"=> null,
  		"country"=>"US",
	  	"first_name"=> null,
  		"last_name"=> null,
	  	"phone"=> "(555)555-5555",
  		"province"=> "KY",
	  	"zip"=>"40202",
	  	"province_code"=> null,
  		"country_code"=> null
	)

);*/


$_POST  = file_get_contents('php://input');
//print_r(json_decode($_POST));die;
if(!empty($_POST))
{	
	$postArray = json_decode($_POST);

	$email           = isset($postArray->email) ? $postArray->email : '';
	$lineItems       = isset($postArray->line_items) ? $postArray->line_items : '';
	$billingAddr     = isset($postArray->billing_address[0]) ? $postArray->billing_address[0] : '';
	$shippingAddr    = isset($postArray->shipping_address) ? $postArray->shipping_address : '';
	$deliverydate    = isset($postArray->delivery_date) ? $postArray->delivery_date : '';
	$orderNotes      = isset($postArray->order_notes) ? $postArray->order_notes : '';

//echo"<pre>";print_r($postArray);die;

	$lineItemDetails = array();

	if(!empty($lineItems))
	{
		$temp = array();
		foreach ($lineItems as $lineItem) {

      		        /*$title    = isset($lineItem->title) ? $lineItem->title : '';
			$quantity = isset($lineItem->quantity) ? $lineItem->quantity : '';
			$price    = isset($lineItem->price) ? $lineItem->price: '';*/

			/*$title    = isset($lineItem[0]->title) ? $lineItem[0]->title : '';
			$quantity = isset($lineItem[0]->quantity) ? $lineItem[0]->quantity : '';
			$price    = isset($lineItem[0]->price) ? $lineItem[0]->price: '';
			$amount   = isset($lineItem[0]->amount) ? $lineItem[0]->amount: '';*/
		
			$title    = isset($lineItem[0]->title) ? $lineItem[0]->title : '';
			$quantity = isset($lineItem[0]->quantity) ? $lineItem[0]->quantity : '';
			$price    = isset($lineItem[0]->price) ? $lineItem[0]->price: '';
			$amount   = isset($lineItem[0]->amount) ? $lineItem[0]->amount: '';

			$temp['title']        = $title;
			$temp['quantity']     = $quantity;
			$temp['price']        = $price;
			$temp['properties'][] = array('name'=>'amount','value' => $amount);

			array_push($lineItemDetails, $temp);			
		} 
	}


	$request                = array();
	$request['draft_order'] = array();

	$request['draft_order']['email'] = $email;

	if(!empty($deliverydate))
	{
	      $request['draft_order']['note_attributes'][] = array('name'=>'delivery_date','value' => $deliverydate);
	}

        
        if(!empty($orderNotes))
	{
	      $request['draft_order']['note_attributes'][] = array('name'=>'order_notes','value' => $orderNotes);
	}

	if(!empty($lineItemDetails))
	{
		$request['draft_order']['line_items'] = $lineItemDetails;
	}
	if(!empty($billingAddr))
	{
		$billingAddrs = isset($billingAddr) ? (array) $billingAddr : $billingAddr;
		$request['draft_order']['billing_address'] = $billingAddrs;
	}
	if(!empty($shippingAddr))
	{
		$shippingAddrs = isset($shippingAddr) ? (array) $shippingAddr : $shippingAddr;
		$request['draft_order']['shipping_address'] = $shippingAddrs;
	}
	
	logEntry(print_r($request,1)); 
//print_r($request);die;
	$result = $shopifyOrderDao->createDraftOrder($request);

	if(isset($result['errors']))
	{
		$errorMessage = '';

		foreach($result['errors'] as $key  => $value) 
		{
			if(is_array($value))
		
        			$errorMessage = $errorMessage." ".$key. " ".$value[0] .", ";
			else
				$errorMessage = $errorMessage." ".$key. " ".$value .", ";
	
        	}

		$response = array('status' => 'error', 'data' => null, 'errorMessage' => rtrim($errorMessage,", ")); 
	        echo json_encode($response);

	}
	else
	{	

		$draftOrderId   = $result['draft_order']['id'];
    		$draftOrderName = $result['draft_order']['name'];

    	        $params = array('draft_order_invoice' => array('bcc'=>array('whittrob000@gmail.com','orders@whittinghammeats.com'), 'subject' => 'Draft Order Invoice - '.$draftOrderName, 'custom_message'=>''));


    		$emailInvoice = $shopifyOrderDao->sendDraftOrderInvoice($draftOrderId, $params);

		$invoiceUrl = $result['draft_order']['invoice_url'];
		$data = array('invoiceUrl' => $invoiceUrl);
		$response   = array('status' => 'success', 'data' => $data, 'errorMessage' => null);
		echo json_encode($response);
	}

}
else
{

    $response =  array('status' => 'error', 'data' => null, 'errorMessage' => "No Post Data"); 
    echo json_encode($response);
}


function getProductDetailsWithTitle($title)
{
    $value = callGraphQlCurl($title);
    return json_decode($value);
}


function callGraphQlCurl($data){
  $curl = curl_init();

  $dataParams = getDataParams($data);

  curl_setopt_array($curl, array(
   // CURLOPT_URL => "https://p80w-devstore-5.myshopify.com/admin/api/graphql.json",
    CURLOPT_URL => "https://whittinghammeats.myshopify.com/admin/api/graphql.json",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $dataParams,
    CURLOPT_HTTPHEADER => array(
      "cache-control: no-cache",
      "content-type: application/graphql;charset=UTF-8",
      "X-Shopify-Access-Token: 43a6e64edfa56e1eed2e370047a57f6c"
    ),
  ));

  $response = curl_exec($curl);
  curl_close($curl);


  return $response;

}

function getDataParams($data){

  // $sku = 'orangatang-cap-5-panel-otang-logo-hat';

  $dataParams = '{
    shop {
      products(first: 1, query:"title:'.$data.'") {
        edges {
          node {
            id
            handle
            title
            tags
            variants(first: 100) {
              edges {
                node {
                  id
                  sku
                }
              }
            }
          }
          cursor
        }
        pageInfo {
          hasNextPage
        }
      }
    }
  }';

  // echo PHP_EOL.$dataParams.PHP_EOL;

  return $dataParams;

}

function getProductId($data)
{
    $dataSplit = explode("/", $data);
    $productId = end($dataSplit);

    return $productId;
}

function getVariantId($data,$sku)
{
    
    $variantId = '';
    
    foreach($data as $value)
    {
    	if(empty($sku))
    	{
    		$variantId = getProductId($value->node->id);
    	}
    	else
    	{
    		if($value->node->sku === $sku)
	        {
	            $variantId = getProductId($value->node->id);
	        }
	        else
	        {
	            continue;
	        }	
    	}
        
    }
    return $variantId;

}

function logEntry($message)
{
    
    global $fileToLog;

    $log = new Logging();
    $log->lfile($fileToLog);
    $log->lwrite($message, true);
    $log->lclose();
}

 /*var data = {
     
      "email" : 'test@test.com',
      "line_items" : [{  
              'title'    : 'Aspire Cleito 120 Tank',
              'quantity' : '2',
              'price   ' : '15.00''
           }],
           
      "shipping_address": {
  		"address1" : "123 Amoebobacterieae St",
  		"address2" : "",
  		"city" : "Ottawa",
  		"company": null,
  		"country":"US",
	  	"first_name": null,
  		"last_name": null,
	  	"phone": "(555)555-5555",
  		"province": "KY",
	  	"zip":"40202",
	  	"province_code": null,
  		"country_code": null
	}
     };*/

